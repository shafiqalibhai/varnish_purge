<?php

/**
 * @file
 *
 * Administrative functions for Varnish integration.
 */

function varnish_admin_purge_all_pages($form, $form_state) {
  $host = array();
  $host = get_sites('');

  $markup = '';
  foreach ($host as $key => $site) {
    $site_parsed = parse_url($site);
    if ($form['pagecache']['custom_url']['#value' . $key] != '' || $form['pagecache']['custom_url' . $key]['#value'] != NULL) {
      varnish_purge($site_parsed['host'], $form['pagecache']['custom_url' . $key]['#value']);
      watchdog('varnish', 'Varnish cache cleared for ' . $site_parsed['scheme'] . '://' . $site_parsed['host'] . $form['pagecache']['custom_url' . $key]['#value'], array(), WATCHDOG_NOTICE);
      $markup .= t('Varnish cache cleared for ' . $site_parsed['scheme'] . '://' . $site_parsed['host'] . $form['pagecache']['custom_url' . $key]['#value'] . '<br />');
    }
    else {
      if ($site_parsed['path'] == '') {
        $site_parsed['path'] = '/';
      }
      varnish_purge($site_parsed['host'], $site_parsed['path']);
      watchdog('varnish', 'Varnish cache cleared for ' . $site_parsed['scheme'] . '://' . $site_parsed['host'] . $site_parsed['path'], array(), WATCHDOG_NOTICE);
      $markup .= t('Varnish cache cleared for ' . $site_parsed['scheme'] . '://' . $site_parsed['host'] . $site_parsed['path'] . '<br />');
    }
  }
  
  return $markup;
}

/**
 * Menu callback for varnish purge admin settings.
 */
function varnish_admin_purge_settings_form() {
  $form = array();
  $sites = get_sites('');
  $sites_list = '';
  foreach ($sites as $site) {
    $sites_list .= '- ' . $site . '<br />';
  }

  $form['sitecache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Cache'),
    '#description' => t('All pages within the following base URLs will be purged from varnish: <br /><br />' . $sites_list . '<br />'),
  );

  $form['sitecache']['add'] = array(
    '#type' => 'button',
    '#value' => t('Clear all varnish caches'),
    '#ajax' => array(
      'callback' => 'varnish_admin_purge_all_pages',
      'wrapper' => 'edit-add',
      'method' => 'after',
      'effect' => 'slide',
    ),
  );

  $form['pagecache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Cache'),
  );
  
  $form['pagecache']['clear_cache_on_node_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear varnish cache on node update for all base URLs'),
    '#default_value' => variable_get('clear_cache_on_node_update', (module_exists('admin_menu')) ? '0' : '1'),
  );

$hosts = get_sites('');
foreach ($hosts as $key => $value) {
  $form['pagecache']['custom_url' . $key] = array(
    '#type' => 'textfield', 
    '#description' => t('Enter a link to clear its cache. eg. /abc/def/'),
    '#default_value' => '/', 
    '#required' => FALSE,
    '#field_prefix' => $value,
  );
}

  
  $form['pagecache']['clear_custom_url'] = array(
    '#type' => 'button',
    '#value' => t('Clear varnish cache'),
    '#ajax' => array(
      'callback' => 'varnish_admin_purge_all_pages',
      'wrapper' => 'edit-clear-custom-url',
      'method' => 'after',
      'effect' => 'slide',
    ),
  );

  //display error message when user has not configured the variables to be used in this module
  if (variable_get('base_url_varnish_purge_all') == '' || variable_get('varnish_ip') == '') {
    drupal_set_message('Please make sure you have configured the following three variables in the settings.php file. <br />     <pre>$conf[\'base_url_varnish_purge_all\'] = \'http://example1.com http://example2.com\';
$conf[\'varnish_ip\'] = \'127.0.0.1:6082 127.1.1.2:6082\';</pre>', $type = 'error', $repeat = FALSE);
    $form['sitecache']['add']['#disabled'] = TRUE;
    $form['pagecache']['clear_cache_on_node_update']['#disabled'] = TRUE;
    if (variable_get('varnish_ip') == '') {
      $form['pagecache']['clear_custom_url']['#disabled'] = TRUE;
    }
  }
  else {
    variable_set('varnish_control_terminal', variable_get('varnish_ip'));
  }

  return system_settings_form($form);
  return $form;
}